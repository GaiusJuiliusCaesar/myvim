# Introduction
This is my personal VIM customized settings. You can download and modify according to your needs.

# Download and Install
- Clone the my git repository as following.
```bash
$ git clone git@gitlab.com:SSeshadriRaja/myvim.git $HOME/.vim
```
- Then, install the **Plugins** as following.
```bash
$ vim +PlugInstall
```
- Now, you can start using the vim

# License
>This program is free software: you can redistribute it and/or modify
>it under the terms of the GNU General Public License as published by
>the Free Software Foundation, either version 3 of the License, or
>(at your option) any later version.

>This program is distributed in the hope that it will be useful,
>but WITHOUT ANY WARRANTY; without even the implied warranty of
>MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
>GNU General Public License for more details.

>You should have received a copy of the GNU General Public License
>along with this program.  If not, see [GNU License](https://www.gnu.org/licenses/)

---
**NOTE:**

>**VIM** and its **Plugins** have its own respective license.
---
