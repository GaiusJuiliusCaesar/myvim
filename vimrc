" =========================================================================== "
" 			My Custom vimrc
" =========================================================================== "
" Created after watching PyCon 2012 Singapore
" https://www.youtube.com/watch?v=YhqsjUUHj6g

" HOW TO INSTALL VIM
"
" Prerequisites:
" (CentOS) sudo yum install gcc-c++ ncurses-devel python-devel vim
" (Ubuntu) sudo apt-get build-dep vim
" (OSX) Command Line Tools for Xcode and brew install vim --with-python3
" (Windows 10) scoop install vim
" -- OR --
" (Windows 10) winget install vim.vim
"
" $ vim --version

" Automatic reloading of vimrc
autocmd! bufwritepost vimrc source %

" Enable spell checking
set spell spelllang=en_us
set complete+=kspell

" Make vim incompatible to vi
set nocompatible

" Set default shell as BASH in Unix/Linux environment
set shell=bash

" Display incomplete commands and menu
set showcmd
set wildmenu

" Mouse and Backspace
set mouse=nvi		" on OSX press ALT and click
" indent  allow backspacing over auto indent
" eol     allow backspacing over line breaks (join lines)
" start   allow backspacing over the start of insert; CTRL-W and CTRL-U
"         stop once at the start of insert.
set backspace=indent,eol,start
set bs=2		" Make backspace behave like normal again

" Flash screen instead of beep sound
set visualbell

" Encoding
set encoding=utf-8

" Set the encoding of files written
set fileencoding=utf-8

" Rebind <Leader> key
let mapleader = ","

" Bind no highlight
" Removes highlight of your search
noremap	<C-n> :nohl<CR>
vnoremap <C-n> :nohl<CR>
inoremap <C-n> :nohl<CR>

" Bind Ctrl+<movement> keys to move around the windows,
" instead of using Ctrl+w + <movement>
map <c-j> <c-w>j
map <c-k> <c-w>k
map <c-l> <c-w>l
map <c-h> <c-w>h

" Easier moving between tabs
map <Leader>n <esc>:tabprevious<CR>
map <Leader>m <esc>:tabnext<CR>

" Set Line numbers, length and colors
set number	" Show line numbers
set tw=80	  " Width of document (used by gd)
set nowrap	" don't automatically wrap on load
set fo-=t	  " don't automatically wrap text when typing

" Disable comment (#) when paste from clipboard
set copyindent

" Size of a hard tab stop "indent" size
"
" 2 spaces
autocmd FileType yaml setlocal ai ts=2 sw=2 et
autocmd FileType html setlocal ai ts=2 sw=2 et
autocmd FileType javascript setlocal ai ts=2 sw=2 et
autocmd FileType puppet setlocal ai ts=2 sw=2 et
autocmd FileType ruby setlocal ai ts=2 sw=2 et
autocmd FileType xml setlocal ai ts=2 sw=2 et
autocmd FileType json setlocal ai ts=2 sw=2 et
autocmd FileType sh setlocal ai ts=2 sw=2 et
autocmd FileType gitcommit setlocal ai ts=2 sw=2 et
autocmd FileType vim setlocal ai ts=2 sw=2 et
autocmd FileType ps1 setlocal ai ts=2 sw=2 et
autocmd FileType apache setlocal ai ts=2 sw=2 et
autocmd FileType conf setlocal ai ts=2 sw=2 et

" 4 spaces
autocmd FileType python setlocal ai ts=4 sw=4 et
autocmd FileType fish setlocal ai ts=4 sw=4 et
autocmd FileType xonsh setlocal ai ts=4 sw=4 et


" Enable syntax highlighting
filetype off
syntax enable
filetype plugin indent on

" Open a new shell scripts, python scripts, yaml etc using custom template.
au bufnewfile *.sh 0r $HOME/.vim/header_templates/sh_header.sh
au bufnewfile *.bash 0r $HOME/.vim/header_templates/sh_header.sh
au bufnewfile *.xsh 0r $HOME/.vim/header_templates/xsh_header.xsh
au bufnewfile *.fish 0r $HOME/.vim/header_templates/fish_header.fish
au bufnewfile *.py 0r $HOME/.vim/header_templates/py_header.py
au bufnewfile *.yaml 0r $HOME/.vim/header_templates/yaml_header.yaml
au bufnewfile *.yml 0r $HOME/.vim/header_templates/yaml_header.yaml
au bufnewfile *.go 0r $HOME/.vim/header_templates/go_header.go
au bufnewfile *.ps1 0r $HOME/.vim/header_templates/ps1_header.ps1

" Easier formatting of paragraphs
vmap Q gq
nmap Q gqap

" Useful settings History and undo levels
set history=1000
set undolevels=1000

" Make search case insensitive
set hlsearch
set incsearch
set ignorecase
set smartcase

" Disable stupid backup and swap files - they trigger too many events
" for file system watchers
set nobackup
set nowritebackup
set noswapfile

" Set viminfo file path to NONE from copying data from your current session.
" Like Ansible-Vault.
set viminfofile=NONE

" Hide mode type
set noshowmode

" Command to save a file with sudo privileges.
command W :execute ':silent w !sudo tee % > /dev/null' | :edit!

" Terminal settings
" set splitbelow
" set termwinsize=10x0
" below terminal

" Specify a directory for plugins
call plug#begin('~/.vim/bundle')

" Shorthand notation for plugin
" -- Making Vim look Good --
Plug 'arcticicestudio/nord-vim'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" -- Vim as a SysAdmin Programmer's text editor --
Plug 'scrooloose/nerdtree'
Plug 'jistr/vim-nerdtree-tabs'
Plug 'Yggdroot/indentLine'
Plug 'godlygeek/tabular'
Plug '907th/vim-auto-save'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'vim-syntastic/syntastic'
" Close brackets automatically (Disable `set paste`)
Plug 'Raimondi/delimitMate'

" -- Python --
Plug 'python-mode/python-mode', { 'for': 'python', 'branch': 'develop' }
Plug 'psf/black'

" -- MarkDown --
Plug 'plasticboy/vim-markdown'

" -- Automation Tools --
Plug 'rodjek/vim-puppet'
Plug 'pearofducks/ansible-vim', { 'do': 'cd ./UltiSnips; ./generate.py' }

" -- Shell Plugins --
Plug 'z0mbix/vim-shfmt', { 'for': 'sh' }
Plug 'dag/vim-fish'
Plug 'linkinpark342/xonsh-vim'
Plug 'pprovost/vim-ps1'

" -- GoLang --
Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }

" -- Nginx --
Plug 'chr4/nginx.vim'

" -- SSL Secure --
Plug 'chr4/sslsecure.vim'

" -- Git Plugins --
Plug 'airblade/vim-gitgutter'
Plug 'tpope/vim-fugitive'

" Initialize plugin system
call plug#end()


" =========================================================================== "
" 			Python IDE Setup
" =========================================================================== "

" Settings for Nord
colorscheme nord
" cd ~/.vim/color
" wget -O wombat256mod.vim \
" http://www.vim.org/scripts/download_script.php?src_id=13400
set colorcolumn=80
highlight ColorColumn ctermbg=233
set t_Co=256

" Settings for vim-airline
" set laststatus=2
if has("statusline") && !&cp
  set laststatus=2              " always show the status bar
  set statusline=%f\ %m\ %r     " file name, modified, read only
  set statusline+=\ %l/%L[%p%%] " current line/total lines
  set statusline+=\ %v[0x%B]    " current column [hex char]
endif

" Settings for vim-airline
let g:airline_detect_paste=0
let g:airline#extensions#tabline#enabled = 1

" Settings for vim-airline-themes
let g:airline#themes#nord#palette = {}
let g:airline_powerline_fonts = 1

" Settings for ctrlp
" cd ~/.vim/bundle
" git clone https://github.com/ctrlpvim/ctrlp.vim.git
let g:ctrlp_max_height = 30
set wildignore+=*.pyc
set wildignore+=*_build/*
set wildignore+=*/coverage/*

" Better navigation through omnicomplete option list
" See http://stackoverflow.com/questions/2170023/how-to-map-keys-for
set completeopt=longest,menuone
function! OmniPopup(action)
  if pumvisible()
    if a:action == 'j'
      return "\<C-N>"
    elseif a:action == 'k'
      return "\<C-P>"
    endif
  endif
  return a:action
endfunction

inoremap <silent><C-j> <C-R>=OmniPopup('j')<CR>
inoremap <silent><C-k> <C-R>=OmniPopup('k')<CR>

" Function to remove whitespace
" https://vi.stackexchange.com/questions/454/whats-the-simplest-way-to-strip-\
" trailing-whitespace-from-all-lines-in-a-file
fun! TrimWhitespace()
  let l:save = winsaveview()
  keeppatterns %s/\s\+$//e
  call winrestview(l:save)
endfun

command! TrimWhitespace call TrimWhitespace()

" Automatically remove white spaces.
autocmd BufWritePre * :call TrimWhitespace()

" -- Python Black Settings --
" Python line length overwrite default 88.
let g:black_linelength = 79

" Run 'black' on save.
autocmd BufWritePre *.py execute ':Black'

" -- Python folding --
" mkdir -p ~/.vim/ftplugin
" wget -O ~/.vim/ftplugin/python_editing.vim \
" http://www.vim.org/scripts/download_script.php?src_id=5492
" Press 'f' to fold the function 'F' to fold all functions
set nofoldenable

" -- Syntastic settings --
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

" Python checker
let g:syntastic_python_checkers = ['isort', 'flake8', 'pycodestyle']

" Bash checker
let g:syntastic_sh_checkers = ['shellcheck']
let g:syntastic_sh_shellcheck_args = "-x"

" YAML checker
let g:syntastic_yaml_checkers = ['yamllint']

" -- IndentLine Settings --
" Enable Indent Lines
let g:indentLine_enabled = 1
" Indent Char List
let g:indentLine_char_list = ['|', '¦', '┆', '┊']

" -- NERDTree Settings --
" Open a NERDTree automatically when vim starts up
autocmd VimEnter * NERDTree | wincmd p
" Close vim if the only window left open is a NERDTree
autocmd BufEnter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
" Show hidden files
let NERDTreeShowHidden=1
" Open NERDTree automatically in tabs
let g:nerdtree_tabs_open_on_console_startup=1

" -- Python-mode Settings --
" Turn on the whole plugin.
let g:pymode = 1
" Trim unused white spaces on save.
let g:pymode_trim_whitespaces = 1
" Setup max line length
let g:pymode_options_max_line_length = 79
" Enable colorcolumn display at max_line_length.
let g:pymode_options_colorcolumn = 1
" By default python-mode uses python 2 syntax checking. Enabling python 3.
let g:pymode_python = 'python3'

" -- Vim-auto-save Settings --
" Enable AutoSave on Vim startup
let g:auto_save = 1
" Do not display the auto-save notification
let g:auto_save_silent = 1
" Events AutoSave will perform
let g:auto_save_events = ["InsertLeave", "TextChanged"]
" Write all open buffers as if you would use :wa
let g:auto_save_write_all_buffers = 1

" -- Ansible-vim Settings --
" Attribute highlight
let g:ansible_attribute_highlight = "ob"
" Name highlight
let g:ansible_name_highlight = 'b'

" -- Vim-Terraform Settings --
" Allow vim-terraform to align settings automatically with Tabularize.
let g:terraform_align=1
" Allow vim-terraform to automatically fold (hide until unfolded) sections of
" terraform code. Defaults to 0 which is off.
let g:terraform_fold_sections=1
" Allow vim-terraform to automatically format *.tf and *.tfvars files with
" terraform fmt. You can also do this manually with the :TerraformFmt command.
let g:terraform_fmt_on_save=1

" -- Vim-Markdown Settings --
" Set folding style like Python-mode
let g:vim_markdown_folding_style_pythonic = 1

" -- Vim-fish Settings --
" Set up :make to use fish for syntax checking.
compiler fish
" Set this to have long lines wrap inside comments.
setlocal textwidth=79
" Enable folding of block structures in fish.
setlocal foldmethod=expr

" -- Vim-go Settings --
" Disable auto loading template.
let g:go_template_autocreate = 0

" -- Vim-shfmt Settings --
" 2 spaces indentation
let g:shfmt_extra_args = '-i 2'
" Auto format on save
let g:shfmt_fmt_on_save = 1

" -- Vim-ps1 Settings --
" The ps1 syntax file provides syntax folding for script blocks and digital
" signatures in scripts.
let g:ps1_nofold_blocks = 1
let g:ps1_nofold_sig = 1

" -- Close brackets automatically --
let g:delimitMate_expand_cr = 2

" -- Code Auto complete --
